import { Alert } from "react-native";
import { doc, deleteDoc } from "firebase/firestore";
import { db } from "../../config/Config";

const ConfirmDeletePost = async (itemId) => {
  console.log("itemid", itemId);

  try {
    // Delete document from "categories" collection
    await deleteDoc(doc(db, "categories", itemId));

    console.log("Post deleted successfully");
  } catch (error) {
    console.log(error);
  }
};

const DeletePost = (itemId) =>
  Alert.alert("Are You Sure?", "Are you sure you want to delete this post?", [
    {
      text: "Cancel",
      onPress: () => console.log(itemId),
    },
    { text: "OK", onPress: () => ConfirmDeletePost(itemId) },
  ]);

export default DeletePost;

