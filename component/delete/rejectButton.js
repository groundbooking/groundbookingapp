// import {  Alert } from "react-native";
// import { doc,deleteDoc} from "firebase/firestore";
// import { db } from "../../config/Config";

// const ConfirmDeletePost = async (itemId) => {
//   console.log("itemid",itemId)

//   try{
//     deleteDoc(doc(db, "categories", itemId));

//   } catch (error){
//     console.log(error)
//   }
// };

// const DeletePost = (itemId) =>

//   Alert.alert("Are You Sure?", "Are you Sure you want to delete this post", [
//     {
//       text: "Cancel",
//       onPress: () => console.log(itemId),
//     },
//     { text: "OK", onPress: () => ConfirmDeletePost(itemId) },
//   ]);

// export default DeletePost;

import { Alert } from "react-native";
import { doc, deleteDoc } from "firebase/firestore";
import { db } from "../../config/Config";

const ConfirmRejectPost = async (item, email) => {
  console.log("itemid", item);
  console.log(email);

  const res = await fetch("https://quickcards-api.vercel.app/zoho-email", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    credentials: "include",
    body: JSON.stringify({
      user: "gyelpozhingbooking@zohomail.com",
      pass: "pematshering.@123",
      to: email, //admin gmail
      subject: "Ground has not been cancelled",
      html: `<b>${item.ground} cancellation rejected!</b>`,
    }),
  });

  res.text().then((text) => console.log(text));
  if (!res.ok) return;

  try {
    // Delete document from "CancelRequestData" collection
    await deleteDoc(doc(db, "CancelRequestData", item));
    console.log("Post deleted successfully");
  } catch (error) {
    console.log(error);
  }
};

const RejectPost = (item,email) => {
  Alert.alert("Are You Sure?", "Are you sure you want to reject this post?", [
    {
      text: "Cancel",
      onPress: () => console.log(item),
    },
    { text: "OK", onPress: () => ConfirmRejectPost(item,email) },
  ]);
};
export default RejectPost;
