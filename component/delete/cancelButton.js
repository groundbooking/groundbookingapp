import { Alert } from "react-native";
import { doc, deleteDoc } from "firebase/firestore";
import { db } from "../../config/Config";

const ConfirmDeletePost = async (item,email) => {
  console.log("itemid", item);
  console.log("email",email)


  const res = await fetch("https://quickcards-api.vercel.app/zoho-email", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    credentials: "include",
    body: JSON.stringify({
      user: "gyelpozhingbooking@zohomail.com",
      pass: "pematshering.@123",
      to: email, //admin gmail
      subject: "Ground cancelled",
      html: `<b>${item.ground} has been cancelled</b>`,
    }),
  });

  res.text().then((text) => console.log(text));
  if (!res.ok) return;

  try {
    // Delete document from "categories" collection
    await deleteDoc(doc(db, "categories", item));

    // Delete document from "CancelRequestData" collection
    await deleteDoc(doc(db, "CancelRequestData", item));

    console.log("Post deleted successfully");
  } catch (error) {
    console.log(error);
  }
};

const DeletePost = (item,email) => {
  Alert.alert("Are You Sure?", "Are you sure you want to delete this post?", [
    {
      text: "Cancel",
      onPress: () => console.log(item),
    },
    { text: "OK", onPress: () => ConfirmDeletePost(item,email) },
  ]);
};
export default DeletePost;
