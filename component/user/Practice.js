import React, { useState, useRef } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Button,
  TextInput,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Picker } from "@react-native-picker/picker";
import {
  addDoc,
  collection,
  query,
  where,
  getDocs,
  doc,
  getDoc,
  setDoc,
} from "firebase/firestore";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Ionicons } from "@expo/vector-icons";

import { db } from "../../config/Config";

const Practice = () => {
  const [groundName, setGroundName] = useState("");
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [time, setTime] = useState(null);
  const [date, setDate] = useState(null);

  const validatePhoneNumber = (phoneNumber) => {
    const phoneNumberRegex = /^17\d{6}$/; // Regex to match 8-digit phone numbers starting with 17
    return phoneNumberRegex.test(phoneNumber);
  };

  const isPastDate = (selectedDate) => {
    const today = new Date();
    const selected = new Date(selectedDate);

    return selected < today;
  };

  const uploadCategory = async () => {
    const email = await AsyncStorage.getItem("Email");

    if (groundName && name && phoneNumber && email && time && date) {
      try {
        if (!validatePhoneNumber(phoneNumber)) {
          Alert.alert(
            "Invalid Phone Number",
            "Please enter a valid phone number starting with 17 and having 8 digits.",
            [{ text: "OK" }]
          );
          return;
        }

        if (isPastDate(date)) {
          Alert.alert(
            "Invalid Date",
            "Booking for past dates is not allowed.",
            [{ text: "OK" }]
          );
          return;
        }
        const querySnapshot = await getDocs(
          query(
            collection(db, "categories"),
            where("Date", "==", date),
            where("Time", "==", time)
          )
        );
        if (!querySnapshot.empty) {
          // Date and time slot is already booked
          Alert.alert(
            "Slot Already Booked",
            "This date and time slot is already booked. Please choose a different one.",
            [{ text: "OK" }]
          );
          return;
        }

        // Get the booking counter document
        const counterDocRef = doc(db, "bookingCounters", "counter");
        const counterDocSnap = await getDoc(counterDocRef);

        // Retrieve the current counter value
        let currentCounter = counterDocSnap.exists()
          ? counterDocSnap.data().counter
          : 1;

        // Generate the booking ID
        const bookingId = `Booking-${currentCounter}`;

        // Increment the counter value
        const newCounter = currentCounter + 1;

        // Update the counter document
        await setDoc(counterDocRef, { counter: newCounter });

        // Proceed with uploading the booking details with the generated ID
        const docRef = await addDoc(collection(db, "categories"), {
          GroundName: groundName,
          Name: name,
          PhoneNumber: phoneNumber,
          Email: email,
          Time: time,
          Date: date,
          BookingId: bookingId,
        });
        Alert.alert("Booking Successful", "Your booking has been confirmed.", [
          { text: "OK" },
        ]);

        // Reset the state values to their initial values
        setGroundName("");
        setName("");
        setPhoneNumber("");
        setTime(null);
        setDate(null);
      } catch (error) {
        console.log(error);
        // Handle error here
      }
    } else {
      // Handle required field validation
      Alert.alert(
        "Incomplete Fields",
        "Please fill in all the required fields.",
        [{ text: "OK" }]
      );
    }
  };
  const pickerRef = useRef();

  const handleDrawerToggle = () => {
    props.navigation.toggleDrawer();
  };

  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Image
        source={require("../../assets/images/Rectangle.png")}
        style={styles.imageBackground}
      />
      <Image
        source={require("../../assets/images/Shape.png")}
        style={styles.shapeImage}
      />
      <View style={styles.drawerHeaderContainer}>
        <TouchableOpacity onPress={handleDrawerToggle}>
          <Ionicons name="menu-outline" size={34} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.headingContainer}>
        <View style={styles.heading}>
          <Text style={styles.headingText}>Book Now</Text>
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="Ground Name"
            value={groundName}
            onChangeText={(text) => setGroundName(text)}
          />
          <TextInput
            style={styles.input}
            placeholder="Name"
            value={name}
            onChangeText={(text) => setName(text, "name")}
          />
          <TextInput
            style={styles.input}
            placeholder="Phone Number"
            value={phoneNumber}
            onChangeText={(text) => setPhoneNumber(text)}
            keyboardType="numeric"
          />
          <TextInput
            style={styles.input}
            placeholder="Date"
            value={date}
            onChangeText={(text) => setDate(text)}
          />
          <Picker
            selectedValue={time}
            style={styles.picker}
            ref={pickerRef}
            onValueChange={(itemValue, itemIndex) => setTime(itemValue)}
            mode="dropdown"
            dropdownIconColor="black"
          >
            <Picker.Item label="Select Time" value="Select Time" />
            <Picker.Item label="04:00 - 06:00 AM" value="04:00 - 06:00 AM" />
            <Picker.Item label="07:00 - 09:00 AM" value="07:00 - 09:00 AM" />
            <Picker.Item label="09:00 - 11:00 AM" value="09:00 - 11:00 AM" />
            <Picker.Item label="12:00 - 02:00 PM" value="12:00 - 02:00 PM" />
            <Picker.Item label="02:00 - 04:00 PM" value="02:00 - 04:00 PM" />
            <Picker.Item label="04:00 - 06:00 PM" value="04:00 - 06:00 PM" />
            <Picker.Item label="06:00 - 08:00 PM" value="06:00 - 08:00 PM" />
            <Picker.Item label="08:00 - 10:00 PM" value="08:00 - 10:00 PM" />
          </Picker>
          <TouchableOpacity
            style={styles.TouchableOpacity}
            onPress={uploadCategory}
          >
            <Text style={styles.touchableText}>Book Now</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Practice;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EEEEEE",
  },
  imageBackground: {
    width: "100%",
    position: "absolute",
    top: 22,
    marginTop: -22,
  },
  shapeImage: {
    position: "absolute",
    top: 22,
    left: 0,
    marginTop: -22,
  },
  headingContainer: {
    alignItems: "center",
    flexDirection: "column",
    flexGrow: 1,
  },
  heading: {
    marginTop: 90,
  },
  headingText: {
    fontSize: 20,
    fontWeight: "bold",
  },
  drawerHeaderContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: -4,
    position: "absolute",
    top: 45,
    left: 10,
  },
  drawerHeaderText: {
    marginLeft: 1000,
    fontSize: 18,
    fontWeight: "bold",
  },
  inputContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: 50,
  },
  input: {
    paddingHorizontal: 2,
    borderWidth: 2,
    marginBottom: 25,
    width: 300,
    color: "black"
  },
  picker: {
    height: 50,
    width: "75%",
    marginHorizontal: "10%",
    borderWidth: 2,
    backgroundColor: "white",
  },
});
