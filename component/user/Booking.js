import React, { useState, useRef } from "react";
import { TextInput } from "@react-native-material/core";
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Pressable,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Picker } from "@react-native-picker/picker";
import {
  addDoc,
  collection,
  query,
  where,
  getDocs,
  doc,
  getDoc,
  setDoc,
} from "firebase/firestore";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Ionicons } from "@expo/vector-icons";
import DateTimePicker from "@react-native-community/datetimepicker";

import { db } from "../../config/Config";

const Booking = (props) => {
  const [groundName, setGroundName] = useState("");
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [time, setTime] = useState(null);
  const [date, setDate] = useState(new Date());
  const [showPicker, setShowPicker] = useState(false);
  const [postDate, setPostDate] = useState("");

  const onDateTextChange = (selectedDate) => {
    if (selectedDate) {
      setDate(selectedDate);
      setShowPicker(false);
      const formattedDate = formatDate(selectedDate);
      setPostDate(formattedDate);
    }
  };

  const formatDate = (rawDate) => {
    let date = new Date(rawDate);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    return `${month}/${day}/${year}`;
  };

  const toggleDatepicker = () => {
    setShowPicker((prev) => !prev);
  };

  const validatePhoneNumber = (phoneNumber) => {
    const phoneNumberRegex = /^17\d{6}$/;
    return phoneNumberRegex.test(phoneNumber);
  };

  const isPastDate = (selectedDate) => {
    const today = new Date();
    const selected = new Date(selectedDate);
    return selected < today;
  };

  const uploadCategory = async () => {
    const email = await AsyncStorage.getItem("Email");

    if (groundName && name && phoneNumber && email && time && date) {
      try {
        if (!validatePhoneNumber(phoneNumber)) {
          Alert.alert(
            "Invalid Phone Number",
            "Please enter a valid phone number starting with 17 and having 8 digits.",
            [{ text: "OK" }]
          );
          return;
        }

        if (isPastDate(date)) {
          Alert.alert(
            "Invalid Date",
            "Booking for past dates is not allowed.",
            [{ text: "OK" }]
          );
          return;
        }

        const querySnapshot = await getDocs(
          query(
            collection(db, "categories"),
            where("Date", "==", formatDate(date)),
            where("Time", "==", time)
          )
        );

        if (!querySnapshot.empty) {
          Alert.alert(
            "Slot Already Booked",
            "This date and time slot is already booked. Please choose a different one.",
            [{ text: "OK" }]
          );
          return;
        }

        const counterDocRef = doc(db, "bookingCounters", "counter");
        const counterDocSnap = await getDoc(counterDocRef);
        let currentCounter = counterDocSnap.exists()
          ? counterDocSnap.data().counter
          : 1;
        const bookingId = `Booking-${currentCounter}`;
        const newCounter = currentCounter + 1;
        await setDoc(counterDocRef, { counter: newCounter });

        const formattedDate = formatDate(date);
        await addDoc(collection(db, "categories"), {
          GroundName: groundName,
          Name: name,
          PhoneNumber: phoneNumber,
          Email: email,
          Time: time,
          Date: formattedDate,
          BookingId: bookingId,
        });

        setGroundName("");
        setName("");
        setPhoneNumber("");
        setTime(null);
        setDate(new Date());

        Alert.alert("Booking Successful", "Your booking has been confirmed.", [
          { text: "OK" },
        ]);
      } catch (error) {
        console.log(error);
        Alert.alert("Error", "An error occurred. Please try again.", [
          { text: "OK" },
        ]);
      }
    } else {
      Alert.alert(
        "Incomplete Fields",
        "Please fill in all the required fields.",
        [{ text: "OK" }]
      );
    }
  };

  const pickerRef = useRef();

  const handleDrawerToggle = () => {
    props.navigation.toggleDrawer();
  };
  return (
    <View style={styles.container}>
      <Image
        source={require("../../assets/images/Rectangle.png")}
        style={styles.imageBackground}
      />
      <Image
        source={require("../../assets/images/Shape.png")}
        style={styles.shapeImage}
      />
      <View style={styles.drawerHeaderContainer}>
        <TouchableOpacity onPress={handleDrawerToggle}>
          <Ionicons name="menu-outline" size={34} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.heading}>
        <Text style={styles.headingText}>Book Now</Text>
      </View>
      <TextInput
        style={styles.input}
        label="GroundName"
        variant="outlined"
        value={groundName}
        onChangeText={(text) => setGroundName(text)}
        labelStyle={styles.inputLabel}
      />
      <TextInput
        style={styles.input}
        label="Name"
        variant="outlined"
        value={name}
        onChangeText={(text) => setName(text)}
        labelStyle={styles.inputLabel}
      />
      <TextInput
        style={styles.input}
        label="PhoneNumber"
        variant="outlined"
        value={phoneNumber}
        onChangeText={(text) => setPhoneNumber(text)}
        labelStyle={styles.inputLabel}
      />
      <View style={styles.date}>
        {showPicker && (
          <DateTimePicker
            mode="date"
            display="spinner"
            value={date}
            onChange={(event, selectedDate) => onDateTextChange(selectedDate)}
          />
        )}
        {!showPicker && (
          <Pressable onPress={toggleDatepicker}>
            <TextInput
              label="Date"
              variant="outlined"
              value={postDate}
              onChangeText={(text) => setDate(text)}
              editable={false}
            />
          </Pressable>
        )}
      </View>
      <Picker
        selectedValue={time}
        style={styles.picker}
        ref={pickerRef}
        onValueChange={(itemValue, itemIndex) => setTime(itemValue)}
        mode="dropdown"
        dropdownIconColor="black"
      >
        <Picker.Item label="Select Time" value="Select Time" />
        <Picker.Item label="04:00 - 06:00 AM" value="04:00 - 06:00 AM" />
        <Picker.Item label="07:00 - 09:00 AM" value="07:00 - 09:00 AM" />
        <Picker.Item label="09:00 - 11:00 AM" value="09:00 - 11:00 AM" />
        <Picker.Item label="12:00 - 02:00 PM" value="12:00 - 02:00 PM" />
        <Picker.Item label="02:00 - 04:00 PM" value="02:00 - 04:00 PM" />
        <Picker.Item label="04:00 - 06:00 PM" value="04:00 - 06:00 PM" />
        <Picker.Item label="06:00 - 08:00 PM" value="06:00 - 08:00 PM" />
        <Picker.Item label="08:00 - 10:00 PM" value="08:00 - 10:00 PM" />
      </Picker>
      <TouchableOpacity
        style={styles.TouchableOpacity}
        onPress={uploadCategory}
      >
        <Text style={styles.touchableText}>Book Now</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Booking;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EEEEEE",
    marginBottom: 45,
  },
  heading: {
    marginBottom: 80,
    height: 30,
  },
  headingText: {
    fontSize: 20,
    fontWeight: "bold",
  },
  input: {
    width: "80%",
    height: 50,
    borderColor: "#D2CDCD",
    borderRadius: 10,
    paddingHorizontal: 10,
    marginBottom: 30,
    color: "#000000",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  picker: {
    height: 50,
    width: "75%",
    marginHorizontal: "10%",
    borderWidth: 2,
    backgroundColor: "white",
  },
  date: {
    width: "75%",
    marginBottom: 12,
  },
  TouchableOpacity: {
    height: "6%",
    width: "50%",
    backgroundColor: "#50C2CA",
    padding: 10,
    borderRadius: 5,
    marginHorizontal: "10%",
    marginTop: 30,
    marginBottom: 30,
  },
  touchableText: {
    color: "#FEF8F8",
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
  },
  imageBackground: {
    width: "100%",
    position: "absolute",
    top: 22,
    marginTop: -22,
  },
  shapeImage: {
    position: "absolute",
    top: 22,
    left: 0,
    marginTop: -22,
  },
  drawerHeaderContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: -50,
    marginRight: "85%",
    marginBottom: 30,
  },
  drawerHeaderText: {
    marginLeft: 1000,
    fontSize: 18,
    fontWeight: "bold",
  },
});
