import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Pressable,
  TouchableOpacity,
  Image,
  Alert,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  getDocs,
  collection,
  query,
  where,
  addDoc,
  setDoc,
  doc,
} from "firebase/firestore";
import { db } from "../../config/Config";
import "firebase/firestore";
import { Ionicons } from "@expo/vector-icons";
import { getAuth, onAuthStateChanged } from "firebase/auth";

const BookingList = (props) => {
  const [data, setData] = useState([]);
  const [loggedInUser, setLoggedInUser] = useState(null);

  const handleDrawerToggle = () => {
    props.navigation.toggleDrawer();
  };

  const getData = async () => {
    const email = await AsyncStorage.getItem("Email");
    return email;
  };

  const handleCancel = async (item) => {
    try {
      const res = await fetch("https://quickcards-api.vercel.app/zoho-email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify({
          user: "gyelpozhingbooking@zohomail.com",
          pass: "pematshering.@123",
          to: "12200013.gcit@rub.edu.bt", //admin gmail
          subject: "Cancellation",
          html: `<b>${item.GroundName} is ready to be cancelled</b>`,
        }),
      });

      res.text().then((text) => console.log(text));
      if (!res.ok) return;

      const q = query(
        collection(db, "CancelRequestData"),
        where("bookingId", "==", item.BookingId),
        where("date", "==", item.Date),
        where("email", "==", item.Email),
        where("name", "==", item.Name),
        where("time", "==", item.Time),
        where("cancelRequest", "==", "false")
      );
      const querySnapshot = await getDocs(q);
      if (!querySnapshot.empty) {
        // Document already exists in CancelRequestData
        Alert.alert(
          "Request Already Sent",
          "The cancellation request has already been sent.",
          [{ text: "OK" }]
        );
        
        return;
      }

      const docRef = await setDoc(
        doc(db, "CancelRequestData",data[0].id),
        {
          bookingId: item.BookingId, // Assuming item.BookingId is the correct property
          date: item.Date,
          email: item.Email,
          name: item.Name,
          time: item.Time,
          cancelRequest: "false",
        }
      );

      Alert.alert(
        "Request Sent",
        "The cancellation request has been sent successfully.",
        [{ text: "OK" }]
      );
    } catch (error) {
      console.log(error);
      // Handle error here
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      setLoggedInUser(await getData());
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const q = query(
          collection(db, "categories"),
          where("Email", "==", loggedInUser)
        );
        const querySnapshot = await getDocs(q);
        const documents = querySnapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        setData(documents);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, [loggedInUser, data]);

  const [user, setUser] = useState(null);

  useEffect(() => {
    const auth = getAuth();

    // Listen for authentication state changes
    const unsubscribe = onAuthStateChanged(auth, (loggedInUser) => {
      setUser(loggedInUser);
    });

    // Clean up the listener when the component unmounts
    return () => unsubscribe();
  }, []);
  const renderGridItem = ({ item }) => (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.Name}>
          <Text style={styles.heading}>Name:</Text>
          <Text style={styles.item}>{item.Name}</Text>
        </View>
        <View style={styles.Email}>
          <Text style={styles.heading}>Email:</Text>
          <Text style={styles.item}>{item.Email}</Text>
        </View>
        <View style={styles.Date}>
          <Text style={styles.heading}>Date:</Text>
          <Text style={styles.item}>{item.Date}</Text>
        </View>
        <View style={styles.Time}>
          <Text style={styles.heading}>Time:</Text>
          <Text style={styles.item}>{item.Time}</Text>
        </View>
      </View>
      <Pressable
        style={styles.cancelButton}
        onPress={() => handleCancel(item)} // Fix: Pass item as a parameter to handleCancel without invoking it
      >
        <Text style={styles.cancelButtonText}>Cancel</Text>
      </Pressable>
    </View>
  );
  

  return (
    <View style={{ flex: 1, marginTop: 50 }}>
      <Image
        source={require("../../assets/images/Rectangle.png")}
        style={styles.imageBackground}
      />
      <Image
        source={require("../../assets/images/Shape.png")}
        style={styles.shapeImage}
      />
      <View style={styles.drawerHeaderContainer}>
        <TouchableOpacity onPress={handleDrawerToggle}>
          <Ionicons name="menu-outline" size={34} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.headingContainer}>
        <Text style={styles.topHeading}>My Booking</Text>
      </View>
      <FlatList
        style={{ height: "100%" }}
        data={data}
        keyExtractor={(item) => item.id}
        numColumns={1}
        renderItem={renderGridItem}
      />
    </View>
  );
};

export default BookingList;

const styles = StyleSheet.create({
  headingContainer: {
    alignItems: "center",
    paddingTop: "12%",
    marginBottom: "9%",
  },
  topHeading: {
    fontWeight: "bold",
    fontSize: 20,
  },

  container: {
    backgroundColor: "#D9D9D9",
    padding: "3%",
    borderRadius: 15,
    margin: "5%",
    height: 380,
    width: "90%",
    marginTop: "8%",
  },
  innerContainer: {
    alignItems: "center",
    flexDirection: "column",
  },
  Name: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: "2%",
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: -4,
    top: 5,
  },
  Email: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: "2%",
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: -4,
    top: 20,
  },
  Date: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: 6,
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: -4,
    top: 35,
  },
  Time: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: 6,
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: -4,
    top: 50,
  },
  heading: {
    fontWeight: "bold",
    color: "#9e9d9d",
    fontSize: 18, // Increase the fontSize to a larger value
  },
  item: {
    fontWeight: "normal",
    fontSize: 16,
    fontWeight: "bold",
  },
  cancelButton: {
    width: 150,
    marginTop: 63,
    marginLeft: 80,
    backgroundColor: "#50C2CA",
    padding: 10,
    borderRadius: 10,
    alignItems: "center",
  },
  cancelButtonText: {
    color: "#FEF8F8",
    fontWeight: "bold",
    fontSize: 20,
  },
  imageBackground: {
    width: "100%",
    position: "absolute",
    top: -28,
    marginTop: -22,
  },
  shapeImage: {
    position: "absolute",
    left: 0,
    top: -28,
    marginTop: -22,
  },
  drawerHeaderContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 12,
    paddingVertical: 4,
  },
  drawerHeaderText: {
    marginLeft: 10,
    fontSize: 50,
    fontWeight: "bold",
  },
});
