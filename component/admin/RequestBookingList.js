import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Pressable,
  Image,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import React, { useEffect, useState } from "react";
import { getDocs, collection } from "firebase/firestore";
import { db } from "../../config/Config";
import "firebase/firestore";

const RequestBookingList = (props) => {
  const handleDrawerToggle = () => {
    props.navigation.toggleDrawer();
  };
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const querySnapshot = await getDocs(collection(db, "categories"));
        const documents = querySnapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        setData(documents);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);

  const acceptBooking = (bookingId) => {
    // Implement the logic to cancel the booking with the given bookingId
    console.log("accept booking with ID:", bookingId);
  };

  const cancelBooking = (bookingId) => {
    // Implement the logic to cancel the booking with the given bookingId
    console.log("Cancel booking with ID:", bookingId);
  };

  const renderGridItem = ({ item }) => (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.Name}>
          <Text style={styles.heading}>Name:</Text>
          <Text style={styles.item}>{item.Name}</Text>
        </View>
        <View style={styles.Email}>
          <Text style={styles.heading}>Email:</Text>
          <Text style={styles.item}>{item.Email}</Text>
        </View>
        <View style={styles.Date}>
          <Text style={styles.heading}>Date:</Text>
          <Text style={styles.item}>{item.Date}</Text>
        </View>
        <View style={styles.Time}>
          <Text style={styles.heading}>Time:</Text>
          <Text style={styles.item}>{item.Time}</Text>
        </View>
      </View>
      <Pressable
        style={styles.acceptButton}
        onPress={() => acceptBooking(item.id)}
      >
        <Text style={styles.acceptButtonText}>Accept Booking</Text>
      </Pressable>
      <Pressable
        style={styles.canceltButton}
        onPress={() => cancelBooking(item.id)}
      >
        <Text style={styles.canceltButtonText}>Cancel Booking</Text>
      </Pressable>
    </View>
  );
  return (
    <View style={{ flex: 1, marginTop: 50 }}>
      <Image
        source={require("../../assets/images/Rectangle.png")}
        style={styles.imageBackground}
      />
      <Image
        source={require("../../assets/images/Shape.png")}
        style={styles.shapeImage}
      />
      <View style={styles.drawerHeaderContainer}>
        <TouchableOpacity onPress={handleDrawerToggle}>
          <Ionicons name="menu-outline" size={34} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.headingContainer}>
        <Text style={styles.topHeading}>Request Booking List</Text>
      </View>

      <FlatList
        style={{ height: "100%" }}
        data={data}
        keyExtractor={(item) => item.id}
        numColumns={1}
        // horizontal={true}
        renderItem={renderGridItem}
      />
    </View>
  );
};

export default RequestBookingList;

const styles = StyleSheet.create({
  headingContainer: {
    alignItems: "center",
    paddingTop: "12%",
    marginBottom: "14%",
  },
  topHeading: {
    fontWeight: "bold",
    fontSize: 20,
  },

  container: {
    backgroundColor: "#D9D9D9",
    padding: "3%",
    borderRadius: 15,
    margin: "5%",
    height: 380,
    width: "90%",
    marginTop: "8%",
  },
  innerContainer: {
    alignItems: "center",
    flexDirection: "column",
  },
  Name: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: "2%",
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: 5,
    top: 5,
  },
  Email: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: 6,
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: 5,
    top: 20,
  },
  Date: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: 6,
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: 5,
    top: 35,
  },
  Time: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: 6,
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: 5,
    top: 50,
  },
  heading: {
    fontWeight: "bold",
    color: "#9e9d9d",
    fontSize: 18, // Increase the fontSize to a larger value
  },
  item: {
    fontWeight: "normal",
    fontSize: 16,
    fontWeight: "bold",
  },
  acceptButton: {
    width: 162,
    marginTop: 63,
    marginLeft: 2,
    backgroundColor: "#50C2CA",
    padding: 10,
    borderRadius: 10,
    alignItems: "center",
  },
  acceptButtonText: {
    color: "#FEF8F8",
    fontWeight: "bold",
    fontSize: 20,
  },
  canceltButton: {
    width: 162,
    marginTop: -46,
    marginLeft: 182,
    backgroundColor: "#50C2CA",
    padding: 10,
    borderRadius: 10,
    alignItems: "center",
  },
  canceltButtonText: {
    color: "#FEF8F8",
    fontWeight: "bold",
    fontSize: 20,
  },
  imageBackground: {
    width: "100%",
    position: "absolute",
    top: -28,
    marginTop: -22,
  },
  shapeImage: {
    position: "absolute",
    left: 0,
    top: -28,
    marginTop: -22,
  },
  drawerHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 4,
  },
  drawerHeaderText: {
    marginLeft: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
});
