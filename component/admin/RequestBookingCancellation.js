import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Pressable,
  Image,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import React, { useEffect, useState } from "react";
import { getDocs, collection, where } from "firebase/firestore";
import { db } from "../../config/Config";
import "firebase/firestore";
import DeletePost from "../delete/cancelButton";
import RejectPost from "../delete/rejectButton";

const RequestBookingCancellation = (props) => {
  const handleDrawerToggle = () => {
    props.navigation.toggleDrawer();
  }; 
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const querySnapshot = await getDocs(
          collection(db, "CancelRequestData"),
          where("cancelRequest", "==", "false")
        );
        const documents = querySnapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        setData(documents);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, [data]);
  // console.log("ugyen");
  const renderGridItem = ({ item }) => (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.Name}>
          <Text style={styles.heading}>Name:</Text>
          <Text style={styles.item}>{item.name}</Text>
        </View>
        <View style={styles.Email}>
          <Text style={styles.heading}>Email:</Text>
          <Text style={styles.item}>{item.email}</Text>
        </View>
        <View style={styles.Date}>
          <Text style={styles.heading}>Date:</Text>
          <Text style={styles.item}>{item.date}</Text>
        </View>
        <View style={styles.Time}>
          <Text style={styles.heading}>Time:</Text>
          <Text style={styles.item}>{item.time}</Text>
        </View>
      </View>
      <Pressable
        style={styles.acceptButton}
        onPress={() => DeletePost(item.id,item.email)}
      >
        <Text style={styles.acceptButtonText}>Accept</Text>
      </Pressable>
      <Pressable
        style={styles.canceltButton}
        onPress={() => RejectPost(item.id,item.email)}
      >
        <Text style={styles.canceltButtonText}>Reject</Text>
      </Pressable>
    </View>
  );
  return (
    <View style={{ flex: 1, marginTop: 50 }}>
      <Image
        source={require("../../assets/images/Rectangle.png")}
        style={styles.imageBackground}
      />
      <Image
        source={require("../../assets/images/Shape.png")}
        style={styles.shapeImage}
      />
      <View style={styles.drawerHeaderContainer}>
        <TouchableOpacity onPress={handleDrawerToggle}>
          <Ionicons name="menu-outline" size={34} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.headingContainer}>
        <Text style={styles.topHeading}>Request Booking Cancellation</Text>
      </View>

      <FlatList
        style={{ height: "100%" }}
        data={data}
        keyExtractor={(item) => item.id}
        numColumns={1}
        // horizontal={true}
        renderItem={renderGridItem}
      />
    </View>
  );
};

export default RequestBookingCancellation;

const styles = StyleSheet.create({
  headingContainer: {
    alignItems: "center",
    paddingTop: "16%",
    marginBottom: "13%",
  },
  topHeading: {
    fontWeight: "bold",
    fontSize: 20,
  },

  container: {
    backgroundColor: "#D9D9D9",
    padding: "3%",
    borderRadius: 15,
    margin: "5%",
    height: 380,
    width: "90%",
    marginTop: "8%",
  },
  innerContainer: {
    alignItems: "center",
    flexDirection: "column",
  },
  Name: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: "2%",
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: -4,
    top: 5,
  },
  Email: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: "2%",
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: -4,
    top: 20,
  },
  Date: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: 6,
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: -4,
    top: 35,
  },
  Time: {
    borderWidth: 2,
    backgroundColor: "#FDF4F4",
    borderColor: "#D2CDCD",
    padding: 6,
    borderRadius: 10,
    width: "90%",
    height: 60,
    marginLeft: -4,
    top: 50,
  },
  heading: {
    fontWeight: "bold",
    color: "#9e9d9d",
    fontSize: 18, // Increase the fontSize to a larger value
  },
  item: {
    fontWeight: "normal",
    fontSize: 16,
    fontWeight: "bold",
  },
  acceptButton: {
    width: "30%",
    marginTop: "19%",
    marginLeft: "10%",
    backgroundColor: "#50C2CA",
    padding: "3%",
    borderRadius: 10,
    alignItems: "center",
  },
  acceptButtonText: {
    color: "#FEF8F8",
    fontWeight: "bold",
    fontSize: 20,
  },
  canceltButton: {
    width: "30%",
    marginTop: "-13%",
    marginLeft: "60%",
    backgroundColor: "#50C2CA",
    padding: "3%",
    borderRadius: 10,
    alignItems: "center",
  },
  canceltButtonText: {
    color: "#FEF8F8",
    fontWeight: "bold",
    fontSize: 20,
  },
  imageBackground: {
    width: "100%",
    position: "absolute",
    top: -28,
    marginTop: -22,
  },
  shapeImage: {
    position: "absolute",
    left: 0,
    top: -28,
    marginTop: -22,
  },
  drawerHeaderContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: -4,
  },
  drawerHeaderText: {
    marginLeft: 10,
    fontSize: 18,
    fontWeight: "bold",
  },
});
