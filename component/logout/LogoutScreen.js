import React, { useEffect } from 'react';
import { Button, View, StyleSheet } from 'react-native';
import { getAuth, signOut } from 'firebase/auth';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const LogoutScreen = () => {
  const navigation = useNavigation();

  useEffect(() => {
    handleLogout(); // Automatically trigger logout on component mount
  }, []);

  const handleLogout = async () => {
    const auth = getAuth();
    try {
      await signOut(auth);
      // Sign-out successful
      await AsyncStorage.removeItem('Email'); // Remove the stored email
      navigation.replace('login');
    } catch (error) {
      // An error happened
      console.log('Logout error:', error);
    }
  };

  return (
    <View>
      {/* You can customize the logout screen UI here */}
      <Button title="Logout" onPress={handleLogout} />
    </View>
  );
};

export default LogoutScreen;
