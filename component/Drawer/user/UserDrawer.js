import * as React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import UserCustomDrawerContents from "./UserCustomDrawerContents";
import BookingList from "../../user/BookingList";
import Booking from "../../user/Booking";
import LogoutScreen from "../../logout/LogoutScreen";
import UserLanding from "../../user/UserLanding";

const Drawer = createDrawerNavigator();

function UserDrawer() {
  return (
    <Drawer.Navigator
      useLegacyImplementation
      drawerContent={(props) => <UserCustomDrawerContents {...props} />}
      screenOptions={{ headerShown: false }}
    >
      {/* <Drawer.Screen name="UserPage" component={UserLanding} /> */}
      <Drawer.Screen name="Booking" component={Booking} />
      <Drawer.Screen name="My Booking" component={BookingList} />
      <Drawer.Screen name="logout" component={LogoutScreen} />
    </Drawer.Navigator>
  );
}
export default UserDrawer;
