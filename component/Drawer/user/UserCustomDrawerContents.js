import * as React from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";

import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from "@react-navigation/drawer";

function UserCustomDrawerContents(props) {
  return (
    <View style={styles.container}>
      <DrawerContentScrollView {...props}>
        {/* <DrawerItem
          label="User Page"
          icon={({ color, size }) => (
            <Ionicons name="person-outline" size={size} color={color} />
          )}
          onPress={() => props.navigation.navigate("UserPage")}
          labelStyle={styles.drawerItemLabel}
          style={styles.drawerItem}
        /> */}
        <DrawerItem
          label="Booking"
          icon={({ color, size }) => (
            <Ionicons name="calendar-outline" size={size} color={color} />
          )}
          onPress={() => props.navigation.navigate("Booking")}
          labelStyle={styles.drawerItemLabel}
          style={styles.drawerItem}
        />
        <DrawerItem
          label="My Booking"
          icon={({ color, size }) => (
            <Ionicons name="list-outline" size={size} color={color} />
          )}
          onPress={() => props.navigation.navigate("My Booking")}
          labelStyle={styles.drawerItemLabel}
          style={styles.drawerItem}
        />
        <DrawerItem
          label="LogOut"
          icon={({ color, size }) => (
            <Ionicons name="log-out-outline" size={size} color={color} />
          )}
          onPress={() => props.navigation.navigate("logout")}
          labelStyle={styles.drawerItemLabel}
          style={styles.drawerItem}
        />
        {/* Add more DrawerItems as needed */}
      </DrawerContentScrollView>

      {/* Add custom footer or additional components if desired */}
    </View>
  );
}

export default UserCustomDrawerContents;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f2ebeb",
  },
  drawerItem: {
    marginBottom: 0, // Increase or decrease the spacing between items
  },
  drawerItemLabel: {
    fontSize: 18,
    fontWeight: "bold",
    color: "black", // Increase the font size of the drawer item labels
  },
});
