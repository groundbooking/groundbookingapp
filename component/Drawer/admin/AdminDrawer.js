import * as React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { StyleSheet } from "react-native";
import AminCustomDrawerContent from "./AminCustomDrawerContent";
import RequestBookingCancellation from "../../admin/RequestBookingCancellation";
import ListOfBooking from "../../admin/ListOFBooking";
import LogoutScreen from "../../logout/LogoutScreen";


const Drawer = createDrawerNavigator();

function AdminDrawer() {
  return (
    <Drawer.Navigator
      useLegacyImplementation
      drawerContent={(props) => <AminCustomDrawerContent {...props} />}
      screenOptions={{headerShown:false}}
    >
      <Drawer.Screen name="Request Booking Cancellation" component={RequestBookingCancellation}/>
      <Drawer.Screen name="List Of Booking" component={ListOfBooking} />
      <Drawer.Screen name="logout" component={LogoutScreen} />
    </Drawer.Navigator>
  );
}
export default AdminDrawer;


