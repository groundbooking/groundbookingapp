import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import BookingList from "../user/BookingList";

const CustomModal = () => {
  const [isModalVisible, setisModalVisible] = useState(false);
  const [chooseData, setChooseData] =useState();
  const changeModalVisible = (bool) => {
    setisModalVisible(bool);
  };
  const setData =(data) => {
    setChooseData(data);
  }
  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        style={styles.touchableOpacity}
        onPress={() => changeModalVisible(true)}
      >
        <Text style={styles.text}>Open Modal</Text>
      </TouchableOpacity>
      <Modal
        transparent={true}
        animationType="fade"
        visible={isModalVisible}
        nRequestClose={() => changeModalVisible(false)}
      >
        <BookingList
          changeModalVisible={changeModalVisible}
          setData={setData}
        />
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   alignItems: "center",
  //   justifyContent: "center",
  //   backgroundColor: "green",
  // },
  touchableOpacity: {
    backgroundColor: "orange",
    paddingHorizontal: 50,
  },
  text: {
    marginVertical: 20,
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default CustomModal;
