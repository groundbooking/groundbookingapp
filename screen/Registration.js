import React, { useState } from "react";
import { auth } from "../config/Config";
import { useNavigation } from "@react-navigation/native";
import {
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from "react-native";
import { db } from "../config/Config";
import { setDoc, doc } from "firebase/firestore";
import { Ionicons } from "@expo/vector-icons";

const Registration = () => {
  const navigation = useNavigation();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirm, setConfirm] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const userDetails = async () => {
    try {
      const docRef = await setDoc(doc(db, "users", email), {
        Name: "Unknown",
        Year: "None",
        Gender: "None",
        Email: email,
        Semester: "None",
      });
    } catch (error) {
      console.log(error.message);
    }
  };

  const createUser = () => {
    if (name === "" || email === "" || password === "" || confirm === "") {
      Alert.alert("All fields are required");
      return;
    }

    setTimeout(() => {
      try {
        auth
          .createUserWithEmailAndPassword(email, password)
          .then((userCredentials) => {
            const user = userCredentials.user;
            userDetails();
            navigation.navigate("login");
          })
          .catch((error) => {
            Alert.alert(error.message);
          });
      } catch (error) {
        Alert.alert(error.message);
      }
    }, 3000);
  };

  return (
    <View style={styles.container}>
      <Image
        style={{ position: "absolute", top: 0, left: 0 }}
        source={require("../assets/images/Shape.png")}
      />
      <Text style={styles.paragraph}>Create an account</Text>
      <View style={styles.paragraphContainer}>
        <Text style={styles.paragraphText}>
          Book your ground through this App{" "}
        </Text>
        <Text style={styles.paragraphText1}>To save time and energy</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.inputBox}
          placeholder="Name"
          value={name}
          onChangeText={setName}
        />
        <Ionicons name="person" size={24} color="#888" style={styles.icon} />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.inputBox}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
        />
        <Ionicons name="mail" size={24} color="#888" style={styles.icon} />
      </View>
      <View style={styles.passwordContainer}>
        <TextInput
          style={styles.passwordInput}
          placeholder="Password"
          value={password}
          onChangeText={setPassword}
          secureTextEntry={!showPassword}
        />
        <Ionicons
          name="ios-lock-closed"
          size={24}
          color="#888"
          style={styles.icon}
        />
        <TouchableOpacity
          style={styles.showPasswordButton}
          onPress={() => setShowPassword(!showPassword)}
        >
          <Ionicons
            name={showPassword ? "ios-eye-off" : "ios-eye"}
            size={24}
            color="white"
          />
        </TouchableOpacity>
      </View>
      <View style={styles.passwordContainer}>
        <TextInput
          style={styles.passwordInput}
          placeholder="Confirm Password"
          value={confirm}
          onChangeText={setConfirm}
          secureTextEntry={!showConfirmPassword}
        />
        <Ionicons
          name="ios-lock-closed"
          size={24}
          color="#888"
          style={styles.icon}
        />
        <TouchableOpacity
          style={styles.showPasswordButton}
          onPress={() => setShowConfirmPassword(!showConfirmPassword)}
        >
          <Ionicons
            name={showConfirmPassword ? "ios-eye-off" : "ios-eye"}
            size={24}
            color="white"
          />
        </TouchableOpacity>
      </View>
      <TouchableOpacity style={styles.button} onPress={() => createUser()}>
        <Text style={styles.buttonText}>Register</Text>
      </TouchableOpacity>
      <Text>
        Already have an account?{" "}
        <Text
          style={{ color: "#51C2CA" }}
          onPress={() => navigation.navigate("login")}
        >
          Sign in
        </Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EEEEEE",
  },
  paragraph: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: 25,
    marginTop: 30,
  },
  paragraphContainer: {
    marginBottom: 30,
  },
  paragraphText: {
    marginBottom: 5,
    fontSize: 14,
    fontWeight: 'bold',
    color: "black"
  },
  paragraphText1: {
    marginBottom: 5,
    marginLeft: 28,
    fontSize: 14,
    fontWeight: 'bold',
    color: "black"
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 18,
    marginRight: 20,
    marginLeft: 20
  },
  icon: {
    position: "absolute",
    left: 10,
  },
  inputBox: {
    flex: 1,
    backgroundColor: "white",
    borderRadius: 10,
    paddingVertical: 13,
    paddingHorizontal: 45,
    fontSize: 18,
    color: "#002f6c",
  },
  passwordContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 15,
    marginLeft: 20,
    marginRight: 20,
  },
  passwordInput: {
    flex: 1,
    backgroundColor: "white",
    borderRadius: 10,
    paddingVertical: 13,
    paddingHorizontal: 45,
    fontSize: 18,
    color: "#002f6c",
  },
  showPasswordButton: {
    position: "absolute",
    right: 10,
    padding: 10,
    backgroundColor: "#51C2CA",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    width: 300,
    backgroundColor: "#51C2CA",
    borderRadius: 10,
    marginVertical: 15,
    paddingVertical: 15,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center",
    fontWeight: 'bold'
  },
});

export default Registration;
