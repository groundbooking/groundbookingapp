// import React, { useState } from "react";
// import {
//   StyleSheet,
//   Image,
//   Text,
//   View,
//   TextInput,
//   TouchableOpacity,
// } from "react-native";
// import AsyncStorage from "@react-native-async-storage/async-storage";

// import { createStackNavigator } from "@react-navigation/stack";
// import { useNavigation } from "@react-navigation/native";

// import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

// const Stack = createStackNavigator();

// function Login() {
//   const navigation = useNavigation();
//   const [email, setEmail] = useState("");
//   const [password, setPassword] = useState("");
//   const [errorMessage, setErrorMessage] = useState(""); // State variable for error message

//   const handleSignIn = () => {
//     const adminEmail = "admin@gmail.com"; // Replace with the admin's email
//     const auth = getAuth();
//     signInWithEmailAndPassword(auth, email, password)
//       .then((userCredential) => {
//         const user = userCredential.user;
//         if (email === adminEmail) {
//           navigation.replace("AdminDrawer");

//         } else {
//           navigation.replace("userdrawer");
//         }
//         saveEmailPass();
//       })
//       .catch((error) => {
//         const errorCode = error.code;
//         const errorMessage = error.message;
//         setErrorMessage("Invalid email or password"); // Set the error message
//       });
//   };
//   const saveEmailPass = async () => {
//     try {
//       await AsyncStorage.setItem("Email", email);

//     } catch (e) {
//       console.log(e);
//     }
//     console.log(email)
//   };

//   return (
//     <View style={styles.container}>
//       <Image
//         style={{ position: "absolute", top: 22, left: 0, marginTop: -22 }}
//         source={require("../assets/images/Shape.png")}
//       />
//       <Text style={styles.paragraph}>Welcome Back!</Text>
//       <Image
//         style={{ marginBottom: 15 }}
//         source={require("../assets/images/image1.png")}
//       />
//       <TextInput
//         style={styles.inputBox}
//         placeholder="Email"
//         value={email}
//         onChangeText={setEmail}
//       />
//       <TextInput
//         style={styles.inputBox}
//         placeholder="Password"
//         value={password}
//         onChangeText={setPassword}
//       />
//       <Text
//         style={{ color: "#51C2CA" }}
//         onPress={() => navigation.navigate("ForgotPassword")}
//       >
//         Forgot Password?
//       </Text>
//       {/* Display error message */}
//       {errorMessage ? (
//         <Text style={styles.errorText}>{errorMessage}</Text>
//       ) : null}
//       <TouchableOpacity style={styles.button} onPress={handleSignIn}>
//         <Text style={styles.buttonText}>Login</Text>
//       </TouchableOpacity>

//       <Text>
//         Don't have an account?{" "}
//         <Text
//           onPress={() => navigation.navigate("registration")}
//           style={{ color: "#51C2CA" }}
//         >
//           Signup
//         </Text>
//       </Text>
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: "center",
//     alignItems: "center",
//     backgroundColor: "#EEEEEE",
//   },
//   paragraph: {
//     fontSize: 18,
//     fontWeight: "bold",
//     textAlign: "center",
//     marginBottom: 20,
//     marginTop: 20,
//   },
//   inputBox: {
//     width: 300,
//     backgroundColor: "white",
//     borderRadius: 20,
//     paddingVertical: 13,
//     paddingHorizontal: 16,
//     fontSize: 16,
//     color: "#002f6c",
//     marginVertical: 15,
//   },
//   button: {
//     width: 300,
//     backgroundColor: "#51C2CA",
//     borderRadius: 10,
//     marginVertical: 15,
//     paddingVertical: 15,
//   },
//   buttonText: {
//     fontSize: 16,
//     fontWeight: "500",
//     color: "#ffffff",
//     textAlign: "center",
//   },
//   errorText: {
//     color: "red",
//     marginTop: 10,
//     fontSize: 16,
//   },
// });

// export default Login;
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from "@react-navigation/native";

import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

const Stack = createStackNavigator();

function Login() {
  const navigation = useNavigation();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState(""); // State variable for error message

  useEffect(() => {
    checkIfUserLoggedIn();
  }, []);

  const checkIfUserLoggedIn = async () => {
    try {
      const storedEmail = await AsyncStorage.getItem("Email");
      if (storedEmail) {
        // User is already logged in, navigate to the appropriate screen
        if (storedEmail === "admin@gmail.com") { // Replace with the admin's email
          navigation.replace("AdminDrawer");
        } else {
          navigation.replace("userdrawer");
        }
      }
    } catch (error) {
      console.log(error);
    }
  };
  const handleSignIn = () => {
    if (email === "" || password === "") {
      setErrorMessage("Please fill in all the required fields");
      return;
    }

    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        if (email === "12200013.gcit@rub.edu.bt") { // Replace with the admin's email
          navigation.replace("AdminDrawer");
        } else {
          navigation.replace("userdrawer");
        }
        saveEmailPass();
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        setErrorMessage("Invalid email or password"); // Set the error message
      });
  };

  const saveEmailPass = async () => {
    try {
      await AsyncStorage.setItem("Email", email);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <Image
        style={{ position: "absolute", top: 22, left: 0, marginTop: -22 }}
        source={require("../assets/images/Shape.png")}
      />
      <Text style={styles.paragraph}>Welcome Back!</Text>
      <Image
        style={{ marginBottom: 15 }}
        source={require("../assets/images/image1.png")}
      />
      <TextInput
        style={styles.inputBox}
        placeholder="Email"
        value={email}
        onChangeText={setEmail}
      />
      <TextInput
        style={styles.inputBox}
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
      />
      <Text
        style={{ color: "#51C2CA" }}
        onPress={() => navigation.navigate("forgetpassword")}
      >
        Forgot Password?
      </Text>
      {/* Display error message */}
      {errorMessage ? (
        <Text style={styles.errorText}>{errorMessage}</Text>
      ) : null}
      <TouchableOpacity style={styles.button} onPress={handleSignIn}>
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>

      <Text>
        Don't have an account?{" "}
        <Text
          onPress={() => navigation.navigate("registration")}
          style={{ color: "#51C2CA" }}
        >
          Signup
        </Text>
      </Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EEEEEE",
  },
  paragraph: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: 20,
    marginTop: 20,
  },
  inputBox: {
    width: 300,
    backgroundColor: "white",
    borderRadius: 20,
    paddingVertical: 13,
    paddingHorizontal: 16,
    fontSize: 16,
    color: "#002f6c",
    marginVertical: 15,
  },
  button: {
    width: 300,
    backgroundColor: "#51C2CA",
    borderRadius: 10,
    marginVertical: 15,
    paddingVertical: 15,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center",
  },
  errorText: {
    color: "red",
    marginTop: 10,
    fontSize: 16,
  },
});

export default Login;
