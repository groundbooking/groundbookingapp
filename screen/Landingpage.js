import React from 'react';
import { StyleSheet,Image, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const  Landingpage= ({navigation}) => {
  return(
    <View style={styles.container}>
      <Image  style={{position:"absolute",top:21,left:0,marginTop:-22}} source={require('../assets/images/Shape.png')}/>
      <Image  style={{marginBottom:15}} source={require('../assets/images/juniorsoccer.png')}/>
      <Text style={styles.paragraph}>Welcome to Gyelpozhing Ground </Text>
      <Text>Book your ground through this App</Text>
      <Text>To save time and engry</Text>
    
      <TouchableOpacity style={styles.button}     
          onPress={() => navigation.replace('login')}
      >
        <Text style={styles.buttonText}>Get Started</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EEEEEE',
  },
  paragraph: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    marginTop:20,
  },
  inputBox: {
    width: 300,
    backgroundColor: 'white',
    borderRadius: 20,
    paddingVertical:13,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#002f6c',
    marginVertical: 15,
  },
  button: {
    width: 300,
    backgroundColor: '#51C2CA',
    borderRadius: 10,
    marginVertical: 15,
    paddingVertical: 15,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center'
  }
});

export default Landingpage;