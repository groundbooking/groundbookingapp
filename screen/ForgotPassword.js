import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  Alert,
  StyleSheet,
  Image,
  Text,
  Pressable,
  Platform,
} from "react-native";
import { getAuth, sendPasswordResetEmail } from "firebase/auth";
import { useNavigation } from "@react-navigation/native";

const ForgotPassword = () => {
  const navigation=useNavigation("");

  const [Email, setEmail] = useState("");
  const handleForgotPassword = async () => {
    try {
      if (Email === "") {
        Alert.alert("Please enter your email");
        return;
      }

      const auth = getAuth();
      sendPasswordResetEmail(auth, Email)
        .then(() => {
          Alert.alert(
            "Password Reset Email Sent",
            "Please check your email to reset your password.",
            [
              {
                text: "OK",
                onPress: () => navigation.navigate("login"),
                style: "default",
              },
            ],
            { cancelable: false }
          );
        })
        .catch((error) => {
          if (error.code === "auth/user-not-found") {
            Alert.alert(
              "User Not Found",
              "Please check your email and try again.",
              [
                {
                  text: "OK",
                  style: "default",
                },
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert(
              "Error",
              error.message,
              [
                {
                  text: "OK",
                  style: "default",
                },
              ],
              { cancelable: false }
            );
          }
        });
    } catch (err) {
      Alert.alert("Error", err.message);
    }
  };

  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/images/Shape.png")}
        style={styles.shapeImage}
      />
      <View style={styles.inputContainer}>
        <Text style={styles.heading}>Forgot Password</Text>
        <Image
          source={require("../assets/images/forgot.png")}
          style={styles.forgotimage}
        />

        <TextInput
          style={styles.input}
          placeholder="Enter your email"
          onChangeText={(text) => setEmail(text)}
          value={Email}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <View>
          <Pressable
            style={styles.resetButton}
            onPress={handleForgotPassword}
          >
            <Text style={styles.resetText}>Reset Password</Text>
          </Pressable>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    marginTop: 50,
    backgroundColor: "#EEEEEE",
  },
  heading: {
    marginBottom: 50,
    marginTop: 30,
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 60
  },
  inputContainer: {
    width: "90%",
    height: 500,
    borderRadius: 10,
    marginTop: 50,
    backgroundColor: "#FFFFFF",
    ...Platform.select({
      android: {
        elevation: 4,
      },
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 4,
      },
    }),
  },
  input: {
    width: "90%",
    height: 45,
    borderWidth: 2,
    borderColor: "#ccc",
    marginBottom: 10,
    padding: 10,
    borderRadius: 10,
    marginLeft: 15
  },
  shapeImage: {
    position: "absolute",
    left: 0,
    top: -28,
    marginTop: -22,
  },
  resetButton: {
    width: "60%",
    marginTop: "8%",
    backgroundColor: "#50C2CA",
    padding: "3%",
    borderRadius: 10,
    alignItems: "center",
    marginLeft: 60
  },
  resetText: {
    color: "#FEF8F8",
    fontWeight: "bold",
    fontSize: 18,
  },
});

export default ForgotPassword;
