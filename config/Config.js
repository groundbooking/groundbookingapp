import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { getFirestore} from 'firebase/firestore';
import  {getStorage} from 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyCx_9zRUb0xqQ9bFFuHNjt7dvYeq7uBhWk",
    authDomain: "gbookingapp.firebaseapp.com",
    projectId: "gbookingapp",
    storageBucket: "gbookingapp.appspot.com",
    messagingSenderId: "202169437385",
    appId: "1:202169437385:web:89f148c1d387a590e01118",
    measurementId: "G-XSBD7W66F6"
};

// Initialize Firebase

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
export{firebase};

const app= firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const db=getFirestore(app);
export const storage=getStorage(app);

