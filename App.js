import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import BookingList from "./component/user/BookingList";
import Landingpage from "./screen/Landingpage";
import ListOfBooking from "./component/admin/ListOFBooking";
import RequestBookingCancellation from "./component/admin/RequestBookingCancellation";
import RequestBookingList from "./component/admin/RequestBookingList";
import UserDrawer from "./component/Drawer/user/UserDrawer";
import AdminDrawer from "./component/Drawer/admin/AdminDrawer";
import Login from "./screen/Login";
import Registration from "./screen/Registration";
import Practice from "./component/user/Practice";
import ForgotPassword from "./screen/ForgotPassword";
import UserLanding from "./component/user/UserLanding";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      {/* <MyDrawer/> */}
      
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Home" component={Landingpage} />
        <Stack.Screen name="login" component={Login} />
        <Stack.Screen name="userdrawer" component={UserDrawer} />
        <Stack.Screen name="AdminDrawer" component={AdminDrawer} />
        <Stack.Screen name="registration" component={Registration} />
        <Stack.Screen name="forgetpassword" component={ForgotPassword} />
        <Stack.Screen name="userLanding" component={UserLanding} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
